/**  
 * 文件名:    DomTest.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午4:49:22  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */
package com.sue.dom;

import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 * @ClassName: DomTest
 * @Description: TODO
 * @author: suxj
 * @date:2015年7月20日 下午4:49:22
 */
public class DomTest {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		sb.append("<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>")
			.append("<cas:authenticationSuccess>")
				.append("<cas:user>09612</cas:user>")
				.append("<cas:attributes>")
					.append("<cas:employeeNumber>09612</cas:employeeNumber>")
				.append("</cas:attributes>")
			.append("</cas:authenticationSuccess>")
			.append("</cas:serviceResponse>");
		
		
		try {
			Document document = DocumentHelper.parseText(sb.toString());
			Element root = document.getRootElement();
			Element authenticationSuccess = root.element("authenticationSuccess");
			Element attributes = authenticationSuccess.element("attributes");
			Element employeeNumber = attributes.element("employeeNumber");
			System.out.println(employeeNumber.getText());
			
			for (Iterator<Element> it = authenticationSuccess.elementIterator(); it.hasNext();) {
				Element element = (Element) it.next();
				System.out.println(element.getName()+":"+element.getText());
		 }  
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	 

}
