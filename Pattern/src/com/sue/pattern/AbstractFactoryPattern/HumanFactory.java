/**  
 * 文件名:    HumanFactory.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:30:55  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: HumanFactory   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:30:55     
 */
public interface HumanFactory {
	
	public Human createYellowHuman();
	
	public Human createWhiteHuman();
	
	public Human createBlackHuman();

}
