/**  
 * 文件名:    AbstractHumanFactory.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:32:25  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: AbstractHumanFactory   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:32:25     
 */
public abstract class AbstractHumanFactory implements HumanFactory {

	protected Human createHuman(HumanEnum huaEnum){
		Human human = null;
		if(!huaEnum.getValue().equals("")){
			try {
				human = (Human)Class.forName(huaEnum.getValue()).newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return human;
	}
}
