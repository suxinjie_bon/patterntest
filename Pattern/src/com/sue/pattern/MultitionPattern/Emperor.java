/**  
 * 文件名:    Emperor.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:46:21  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.MultitionPattern;

import java.util.ArrayList;
import java.util.Random;

/**  
 * @ClassName: Emperor   
 * @Description: 皇帝  
 * @author: suxj  
 * @date:2015年7月21日 上午8:46:21     
 */
public class Emperor {
	
	private static int maxNumberOfEmperor = 2;
	private static ArrayList<String> emperorInfoList = new ArrayList<String>(maxNumberOfEmperor);
	private static ArrayList<Emperor> emperorList = new ArrayList<Emperor>(maxNumberOfEmperor);
	private static int countNumberOfEmperor = 0;
	
	static{
		for(int i=0 ;i<maxNumberOfEmperor ;i++){
			emperorList.add(new Emperor("皇"+(i+1)+"帝"));
		}
	}
	
	private Emperor(){}
	
	public Emperor(String info){
		emperorInfoList.add(info);
	}
	
	public static Emperor getInstance(){
		Random random = new Random();
		countNumberOfEmperor = random.nextInt(maxNumberOfEmperor);
		return (Emperor) emperorList.get(countNumberOfEmperor);
	}
	
	public static void emperorInfo(){
		System.out.println(emperorInfoList.get(countNumberOfEmperor));
	}

}
