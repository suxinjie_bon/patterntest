/**  
 * 文件名:    BlockEnemy.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:08:19  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: BlockEnemy   
 * @Description: 阻击阴兵  
 * @author: suxj  
 * @date:2015年7月20日 下午6:08:19     
 */
public class BlockEnemy implements IStrategy {

	@Override
	public void operate() {
		System.out.println("孙夫人断后，挡住追兵"); 
	}

}
