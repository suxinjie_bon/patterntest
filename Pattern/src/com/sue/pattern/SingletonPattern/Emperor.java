/**  
 * 文件名:    Emperor.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:31:26  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.SingletonPattern;

/**  
 * @ClassName: Emperor   
 * @Description: 皇帝  
 * @author: suxj  
 * @date:2015年7月21日 上午8:31:26     
 */
public class Emperor {
	
//	这种写法存在问题。B/S中一个Http会创建一个新线程，存在并发问题
//	private static Emperor emperor = null;
//	
//	private Emperor(){}
//	
//	public static Emperor getInstance(){
//		if(emperor == null ){
//			emperor = new Emperor();
//		}
//		return emperor;
//	}
	
	private static final Emperor emperor = new Emperor();
	
	private Emperor(){}
	
	public synchronized static Emperor getInstance(){
		return emperor;
	}
	
	public static void emperorInfo(){
		System.out.println("我就是皇帝某某某....");
	}
	
}
