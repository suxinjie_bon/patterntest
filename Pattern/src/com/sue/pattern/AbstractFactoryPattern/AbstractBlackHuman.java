/**  
 * 文件名:    AbstractBlackHuman.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:14:08  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: AbstractBlackHuman   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:14:08     
 */
public abstract class AbstractBlackHuman implements Human {

	@Override
	public void laugh() {
		System.out.println("黑人会笑"); 
	}

	@Override
	public void cry() {
		System.out.println("黑人会哭"); 
	}

	@Override
	public void talk() {
		System.out.println("黑人可以说话，一般人听不懂"); 
	}

}
