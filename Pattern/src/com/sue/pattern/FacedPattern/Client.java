/**  
 * 文件名:    Client.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午5:42:49  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FacedPattern;

/**  
 * @ClassName: Client   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午5:42:49     
 */
public class Client {
	
	public static void main(String[] args) {
		
		ModenPostOffice hellRoadPostOffice  = new ModenPostOffice();
		
		String address = "Happy Road No. 666,God Province,Heaven";
		String context = "Hello,It's me,do you know who I am? I'm your old lover. I'd like to...."; 
		
		hellRoadPostOffice.sendLetter(context, address);
	}
}
