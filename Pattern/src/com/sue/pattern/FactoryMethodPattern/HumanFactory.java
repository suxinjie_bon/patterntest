package com.sue.pattern.FactoryMethodPattern;

import java.util.HashMap;

public class HumanFactory {
	
	//延迟初始化
	private static HashMap<String ,Human> humans = new HashMap<String ,Human>();

	public static Human createHuman(Class clazz){
		
		Human human = null;
		
		try {
			if(humans.containsKey(clazz.getSimpleName())){
				human = humans.get(clazz.getSimpleName());
			}else{
				human = (Human) Class.forName(clazz.getName()).newInstance();
				humans.put(clazz.getSimpleName(), human);
			}
		} catch (InstantiationException e) {
			System.out.println("必须指定人类的颜色"); 
		} catch (IllegalAccessException e) {
			System.out.println("人类定义错误！"); 
		} catch (ClassNotFoundException e) {
			System.out.println("混蛋，你指定的人类找不到！"); 
		}
		return human;
	}
}
