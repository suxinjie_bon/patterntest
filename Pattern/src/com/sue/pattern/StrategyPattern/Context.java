/**  
 * 文件名:    Context.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:09:10  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: Context   
 * @Description: 锦囊  
 * @author: suxj  
 * @date:2015年7月20日 下午6:09:10     
 */
public class Context {
	
	private IStrategy strategy;
	public Context(IStrategy strategy){
		this.strategy = strategy;
	}
	
	public void operate(){
		this.strategy.operate();
	}

}
