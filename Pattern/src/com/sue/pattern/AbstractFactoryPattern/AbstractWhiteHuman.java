/**  
 * 文件名:    AbstractWhiteHuman.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:12:44  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: AbstractWhiteHuman   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:12:44     
 */
public abstract class AbstractWhiteHuman implements Human {

	@Override
	public void laugh() {
		System.out.println("白色人类会大笑，侵略的笑声"); 
	}

	@Override
	public void cry() {
		System.out.println("白色人类会哭"); 
	}

	@Override
	public void talk() {
		System.out.println("白色人类会说话，一般都是但是单字节！"); 
	}
	
}
