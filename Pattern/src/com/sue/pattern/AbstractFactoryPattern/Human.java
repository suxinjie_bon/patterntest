/**  
 * 文件名:    Human.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午12:26:31  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: Human   
 * @Description: 人类  
 * @author: suxj  
 * @date:2015年7月21日 下午12:26:31     
 */
public interface Human {
	
	public void laugh();
	
	public void cry();
	
	public void talk();
	
	public void sex();

}
