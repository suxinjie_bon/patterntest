/**  
 * 文件名:    WangPo.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:20:28  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.ProxyPattern;

/**  
 * @ClassName: WangPo   
 * @Description: TODO  
 * @author: 王婆作为女人的代理：  老鸨
 * @date:2015年7月21日 上午8:20:28     
 */
public class WangPo implements KindWomen {
	
	private KindWomen kindWomen;
	
	public WangPo(){
		this.kindWomen = new PanJinLian();
	}
	
	public WangPo(KindWomen kindWomen){
		this.kindWomen = kindWomen;
	}

	@Override
	public void makeEyesWithMan() {
		this.kindWomen.makeEyesWithMan();
	}

	@Override
	public void happyWithMan() {
		this.kindWomen.happyWithMan();
	}

}
