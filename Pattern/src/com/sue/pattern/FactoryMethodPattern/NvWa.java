/**  
 * 文件名:    NvWa.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午12:33:46  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FactoryMethodPattern;

/**  
 * @ClassName: NvWa   
 * @Description: 女娲造人  
 * @author: suxj  
 * @date:2015年7月21日 下午12:33:46     
 */
public class NvWa {

	public static void main(String[] args) {
		System.out.println("------------造出的第一批人是这样的：白人 -----------------");
		Human whiteHuman = HumanFactory.createHuman(WhiteHuman.class);
		whiteHuman.cry();
		whiteHuman.laugh();
		whiteHuman.talk();
		
		System.out.println("------------造出的第二批人是这样的：黑人 -----------------");
		Human blackHuman = HumanFactory.createHuman(BlackHuman.class);
		blackHuman.cry();
		blackHuman.laugh();
		blackHuman.talk(); 
		
		System.out.println("------------造出的第三批人是这样的：黄色人类 -----------------");
		Human yellowHuman = HumanFactory.createHuman(YellowHuman.class);
		yellowHuman.cry();
		yellowHuman.laugh();
		yellowHuman.talk();
	}
}
