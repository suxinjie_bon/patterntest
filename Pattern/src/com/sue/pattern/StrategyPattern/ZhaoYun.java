/**  
 * 文件名:    ZhaoYun.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:10:31  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: ZhaoYun   
 * @Description: 赵云  
 * @author: suxj  
 * @date:2015年7月20日 下午6:10:31     
 */
public class ZhaoYun {

	public static void main(String[] args) {
		Context context ;
		
		System.out.println("-----------刚刚到吴国的时候拆第一个-------------");
		context = new Context(new BackDoor());
		context.operate();
		
		System.out.println("-----------刘备乐不思蜀了，拆第二个了-------------");
		context = new Context(new GivenGreenLight());
		context.operate();
		
		System.out.println("-----------孙权的小兵追了，咋办？拆第三个 -------------");
		context = new Context(new BlockEnemy());
		context.operate();
	}
}
