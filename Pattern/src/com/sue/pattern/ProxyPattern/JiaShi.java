/**  
 * 文件名:    JiaShi.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:25:49  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.ProxyPattern;

/**  
 * @ClassName: JiaShi   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 上午8:25:49     
 */
public class JiaShi implements KindWomen {

	@Override
	public void makeEyesWithMan() {
		System.out.println("贾氏抛媚眼"); 
	}

	@Override
	public void happyWithMan() {
		System.out.println("贾氏正在Happy中......"); 
	}

}
