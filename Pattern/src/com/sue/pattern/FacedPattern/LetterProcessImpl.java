/**  
 * 文件名:    LetterProcessImpl.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午5:41:47  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FacedPattern;

/**  
 * @ClassName: LetterProcessImpl   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午5:41:47     
 */
public class LetterProcessImpl implements LetterProcess {

	@Override
	public void writeContext(String context) {
		System.out.println("填写信的内容...." + context); 
	}

	@Override
	public void fillEnvelope(String address) {
		System.out.println("填写收件人地址及姓名...." + address); 
	}

	@Override
	public void letterIntoEnvelope() {
		System.out.println("把信放到信封中...."); 
	}

	@Override
	public void sendLetter() {
		System.out.println("邮递信件..."); 
	}

}
