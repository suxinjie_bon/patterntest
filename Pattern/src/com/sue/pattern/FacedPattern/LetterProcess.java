/**  
 * 文件名:    LetterProcess.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午5:39:24  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FacedPattern;

/**  
 * @ClassName: LetterProcess   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午5:39:24     
 */
public interface LetterProcess {

	public void writeContext(String context);
	
	public void fillEnvelope(String address);
	
	public void letterIntoEnvelope();
	
	public void sendLetter();
}
