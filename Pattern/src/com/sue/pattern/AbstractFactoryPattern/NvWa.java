/**  
 * 文件名:    NvWa.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:41:25  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: NvWa   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:41:25     
 */
public class NvWa {

	public static void main(String[] args) {
		HumanFactory maleHumanFactory = new MaleHumanFactory();
		HumanFactory femaleHumanFactory =  new FemaleHumanFactory();
		
		Human maleYellowHuman = maleHumanFactory.createYellowHuman();
		Human femaleYellowHuman = femaleHumanFactory.createYellowHuman();
		
		maleYellowHuman.sex();
		maleYellowHuman.cry();
		maleYellowHuman.laugh();
		maleYellowHuman.talk();
		System.out.println("=====================");
		femaleYellowHuman.sex();
		femaleYellowHuman.cry();
		femaleYellowHuman.laugh();
		femaleYellowHuman.talk();
	}
}
