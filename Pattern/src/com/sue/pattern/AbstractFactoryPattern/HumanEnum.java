package com.sue.pattern.AbstractFactoryPattern;

public enum HumanEnum {
	
	YellowMaleHuman("com.sue.pattern.AbstractFactoryPattern.YellowMaleHuman"),
	YellowFemaleHuman("com.sue.pattern.AbstractFactoryPattern.YellowFemaleHuman"),
	WhiteMaleHuman("com.sue.pattern.AbstractFactoryPattern.WhiteMaleHuman"),
	WhiteFemaleHuman("com.sue.pattern.AbstractFactoryPattern.WhiteFemaleHuman"),
	BlackMaleHuman("com.sue.pattern.AbstractFactoryPattern.BlackMaleHuman"),
	BlackFemaleHuman("com.sue.pattern.AbstractFactoryPattern.BlackFemaleHuman");
	
	private String value = "";
	private HumanEnum(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}

}
