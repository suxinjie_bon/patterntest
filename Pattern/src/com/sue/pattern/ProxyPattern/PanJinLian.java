/**  
 * 文件名:    PanJinLian.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:19:09  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.ProxyPattern;

/**  
 * @ClassName: PanJinLian   
 * @Description: 潘金莲  
 * @author: suxj  
 * @date:2015年7月21日 上午8:19:09     
 */
public class PanJinLian implements KindWomen {

	@Override
	public void makeEyesWithMan() {
		System.out.println("潘金莲抛媚眼"); 
	}

	@Override
	public void happyWithMan() {
		System.out.println("潘金莲在和男人做那个....."); 
	}

}
