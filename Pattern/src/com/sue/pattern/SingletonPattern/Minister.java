/**  
 * 文件名:    Minister.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:33:48  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.SingletonPattern;

/**  
 * @ClassName: Minister   
 * @Description: 大臣  
 * @author: suxj  
 * @date:2015年7月21日 上午8:33:48     
 */
public class Minister {

	public static void main(String[] args) {
		Emperor emperor1 = Emperor.getInstance();
		emperor1.emperorInfo();
		
		Emperor emperor2 = Emperor.getInstance();
		Emperor.emperorInfo();
		
		Emperor emperor3 = Emperor.getInstance();
		emperor2.emperorInfo();
	}
	
}
