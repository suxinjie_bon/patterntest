/**  
 * 文件名:    Minister.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:56:22  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.MultitionPattern;

/**  
 * @ClassName: Minister   
 * @Description: 大臣  
 * @author: suxj  
 * @date:2015年7月21日 上午8:56:22     
 */
public class Minister {

	public static void main(String[] args) {
		int ministerNum = 10;
		
		for(int i=0 ;i<ministerNum ;i++){
			Emperor emperor = Emperor.getInstance();
			System.out.print("第"+(i+1)+"个大臣参拜的是：");
			emperor.emperorInfo();   
		}
	}
	
}
