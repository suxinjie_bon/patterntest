/**  
 * 文件名:    KindWomen.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:17:08  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */
package com.sue.pattern.ProxyPattern;

/**
 * @ClassName: KindWomen
 * @Description: 定义一种类型的女人，王婆和潘金莲都是这种类型的女人
 * @author: suxj
 * @date:2015年7月21日 上午8:17:08
 */
public interface KindWomen {
	
	public void makeEyesWithMan();// 抛媚眼

	public void happyWithMan();// you konw that
	
}
