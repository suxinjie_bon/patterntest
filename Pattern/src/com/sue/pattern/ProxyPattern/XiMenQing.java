/**  
 * 文件名:    XiMenQing.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 上午8:23:46  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.ProxyPattern;

/**  
 * @ClassName: XiMenQing   
 * @Description: 西门官人  
 * @author: suxj  
 * @date:2015年7月21日 上午8:23:46     
 */
public class XiMenQing {
	
	public static void main(String[] args){
		
//		WangPo wangPo = new WangPo();
//		//实际是潘金莲在爽
//		wangPo.happyWithMan();
//		wangPo.makeEyesWithMan();
		
		JiaShi jiaShi = new JiaShi();
		WangPo wangPo = new WangPo(jiaShi);
		wangPo.happyWithMan();
		wangPo.makeEyesWithMan();
		
	}
}
