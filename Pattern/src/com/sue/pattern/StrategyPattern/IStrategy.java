/**  
 * 文件名:    IStrategy.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:05:27  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: IStrategy   
 * @Description: 策略接口  
 * @author: suxj  
 * @date:2015年7月20日 下午6:05:27     
 */
public interface IStrategy {

	public void operate();
}
