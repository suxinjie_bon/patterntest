/**  
 * 文件名:    BlackHuman.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午12:29:35  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FactoryMethodPattern;

/**  
 * @ClassName: BlackHuman   
 * @Description: 黑色人种  
 * @author: suxj  
 * @date:2015年7月21日 下午12:29:35     
 */
public class BlackHuman implements Human {

	@Override
	public void laugh() {
		System.out.println("黑人会笑"); 
	}

	@Override
	public void cry() {
		System.out.println("黑人会哭"); 
	}

	@Override
	public void talk() {
		System.out.println("黑人可以说话，一般人听不懂"); 
	}

}
