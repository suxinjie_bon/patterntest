/**  
 * 文件名:    WhiteMaleHuman.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午2:17:10  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.AbstractFactoryPattern;

/**  
 * @ClassName: WhiteMaleHuman   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午2:17:10     
 */
public class WhiteMaleHuman extends AbstractWhiteHuman {

	@Override
	public void sex() {
		System.out.println("该白种人的性别为男...."); 
	}

}
