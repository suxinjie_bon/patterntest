/**  
 * 文件名:    GivenGreenLight.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:07:41  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: GivenGreenLight   
 * @Description: 开绿灯  
 * @author: suxj  
 * @date:2015年7月20日 下午6:07:41     
 */
public class GivenGreenLight implements IStrategy {

	@Override
	public void operate() {
		System.out.println("求吴国太开个绿灯,放行！");
	}

}
