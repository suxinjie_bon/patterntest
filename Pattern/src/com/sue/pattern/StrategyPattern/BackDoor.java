/**  
 * 文件名:    BackDoor.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月20日 下午6:06:35  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月20日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.StrategyPattern;

/**  
 * @ClassName: BackDoor   
 * @Description: 开后门  
 * @author: suxj  
 * @date:2015年7月20日 下午6:06:35     
 */
public class BackDoor implements IStrategy {

	@Override
	public void operate() {
		System.out.println("找乔国老帮忙，让吴国太给孙权施加压力");
	}

}
