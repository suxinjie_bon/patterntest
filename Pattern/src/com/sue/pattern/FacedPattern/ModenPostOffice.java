/**  
 * 文件名:    ModenPostOffice.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午5:43:37  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FacedPattern;

/**  
 * @ClassName: ModenPostOffice   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午5:43:37     
 */
public class ModenPostOffice {
	
	private LetterProcess letterProcess = new LetterProcessImpl();
	private Police letterPolice = new Police();
	
	public void sendLetter(String context ,String address){
		
		letterProcess.writeContext(context);
		letterProcess.fillEnvelope(address);
		letterProcess.letterIntoEnvelope();
		letterPolice.checkLetter(letterProcess);
		letterProcess.sendLetter();
		
	}
}
