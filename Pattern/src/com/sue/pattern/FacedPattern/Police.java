/**  
 * 文件名:    Police.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午5:47:59  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FacedPattern;

/**  
 * @ClassName: Police   
 * @Description: TODO  
 * @author: suxj  
 * @date:2015年7月21日 下午5:47:59     
 */
public class Police {

	public void checkLetter(LetterProcess letterProcess){
		System.out.println("police 检查信件....");
	}
}
