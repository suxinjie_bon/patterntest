/**  
 * 文件名:    YellowHuman.java  
 * 描述:      
 * 作者:      suxj
 * 版本:      1.0  
 * 创建时间:  2015年7月21日 下午12:27:44  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 * 2015年7月21日        suxj     1.0     1.0 Version  
 */ 
package com.sue.pattern.FactoryMethodPattern;

/**  
 * @ClassName: YellowHuman   
 * @Description: 黄色人种  
 * @author: suxj  
 * @date:2015年7月21日 下午12:27:44     
 */
public class YellowHuman implements Human {

	@Override
	public void laugh() {
		System.out.println("黄色人类会大笑，幸福呀！");
	}

	@Override
	public void cry() {
		System.out.println("黄色人类会哭");
	}

	@Override
	public void talk() {
		System.out.println("黄色人类会说话，一般说的都是双字节");
	}

}
